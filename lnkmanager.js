module.exports.TLnkManager = TLnkManager;
module.exports.TSerialPort = TSerialPort;
//класс мастер (инициатор запросов к оборудованию) включающий
//1. настройку последовательного порта
//      1.1. название порта COMxx или dev/tty
//      1.2. скорость связи
//      1.3. кол-во битов: стартовых, данных, стоповых 
//2. Slots[] массив слотов-подписок, каждый элемент которого включает
//      1. Статус:
//          1.1. ErrTimeOut - не дождался ответа
//          1.2. ErrCRC - получен ответ с битым CRC
//      2. Управление слотом
//          2.1. Skip - если 1 пропустить слот
//          2.2. NoRespond - если 1 команда не требует ответа (перейти на следующий слот)
//          2.3. Change - автом-ки устанвливается в 1 после цикла (запрос-ответ) после перпекл на след слот, ставится в 0 
//      3. Временные характеристики
//          3.1. TimeOut время тайм-аута (если  ответ не получен, выставляет ErrTimeOut и переход на след слот)
//          3.2. BeforeWriteTime - время перед отправкой команды
//      4. Out[] - массив который требуется передать
//      5. In[]  - массив принятый от устройства (если нет ошибок)
//      3. Callback - функция которая вызывается по завершению цикла запрос-ответ(с ошибкой или без)
//3. Таймер
//   1) Таймер запускается на время BeforeWriteTime перед командой записи
//      (в основном требуется дла переключения драйвера приёмопередатчика)
//      После срабатывания BeforeWriteTime производится команда записи с блокирующей drain
//   Если команда требует ответа (NoRespond = 0) ну как и большинство команд, то 
//   таймер заряжается на время TimeOut, в противном случает перехожу на следующий слот
//   3) Таймер запускается на время TimeOut после команды записи (запись через drain для того чтобы вседанные ушли)
//   4) При отсчёте TimeOut, возможные ситуации:
//      4.1) таймер досчитал до конца - значит девайс не ответил (сообщаю подписчику и перехожу на следующий слот)
//      4.2) если девайс ответил, то он останавливает таймер 
function TLnkSlot (data, callback){
    this.state = {//ошибки связи
        ErrPort:    false, //ошибка последовательного порта (например, он закрыт)
        ErrTimeOut: false,//ответ устройства не получен
        ErrCRC:     false, //ответ есть но неправильная контрольная сумма
        ErrStr:     ''//строка содержащая описание ошибки
    }
    this.settings = {
        TimeOut:            200, //время ожидания ответа в милисекундах
        BeforeWriteTime:      2, //задержка перед отправкой следующего запроса
        Skip:       false, //true - пропустить слот (и перейти к следующему)
        NoRespond:  false, //true - команда в out не требует ответа на неё (пропускаю ожидание ответа)
        Change:     false  //true - перейти к следующему слоту
    }
    this.name = data.name; //имя слота для идентификации
    this.out = data.cmd;//массив с данными (командой) для передачи в устройство
    this.in  = [];//массив с данными полученными от устройства
    this.onRead = callback;//функция которую требуется вызвать по получению даных от устройства (даже если есть ошибки)
}

//манагер связи
function TLnkManager (settings) {
    this.aSlots = [];//массив слотов
    this.port = settings.port;//коммуникационный порт привязанный к TLnkManager
    this.timerOutID = null;//ID таймера отсчёта TimeOut
    this.timerOpenPortID = null;//ожидание открытия порта
    //
    var index = 0;//индекс текущего слота в массиве aSlots[]
    var self = this;
    //вызывается когда данные от порта получены
    this.onDataRead = function (data, err) {
        //console.log('TLnkManager.onDataRead:'+err);
        var slot = this.aSlots[index];//активный слот
        if (err == undefined) {//получен ответ
            slot.state.ErrTimeOut = false;//cообщаю что небыло тайм аута
            clearTimeout(this.timerOutID);//отменить даймер тайм-аута
        }
        else {//получил тайм-аут
            slot.state.ErrTimeOut = true;//cообщаю что был тайм аут
        }
        //вызвать callback-функцию слота
        if (slot.onRead != null) slot.onRead(data, slot);
        //переключится на следующий слот
        slot.settings.Change = true;//
        this.run('AfterDataRead');
    }
    //установка обработчика события поступления данных в порт
    this.port.sp.on('data',  this.onDataRead.bind(self));
    //добавить слот
    this.addSlot = function (data, callback){
        //console.log('TLnkManager.addSlot');
        var slot = new TLnkSlot(data, callback);//создаю новый слот
        this.aSlots.push(slot);//добавляю его в массив слотов
    }

    //удалить слот
    this.getSlotByName = function (name){
        //console.log('TLnkManager.getSlotByName:'+name);
        var i = this.aSlots.length;
        while (i-- != 0) {
            if (this.aSlots[i].name == name)
                return this.aSlots[i];
        }
        return null;    //раз сюда пришёл, значит в массиве нет искомого ключа
    }

    //удалить слот
    this.delSlot = function (){
        //console.log('TLnkManager.delSlot');
    }
    
    //запускает Линк Манагер в автоматическую работу
    this.chekPortOpen = function(){
        if (this.port.isOpen) {
            this.run('PortOpen');
        }
        else {
            this.timerOpenPortID= setTimeout (this.chekPortOpen.bind(self),1000);
        }
    }

    this.start = function (){
        //console.log('TLnkManager.start');
        this.chekPortOpen();
    }
    
    //останавливает Линк Манагер
    this.stop = function (){
        //console.log('TLnkManager.stop');
    }

    //автоматический обработчик 
    this.run = function (arg){
        //console.log('TLnkManager.run:'+arg);
        if (this.port.isOpen) {//порт открыт можно работать
            //проверка на наличие заполненных слотов
            if (this.aSlots.length == 0) {//если нет заполненных слотов то возвращаюсь сюда в ожидании слотов
                this.timerOutID = setTimeout (this.run.bind(self), 1000, 'WaitSlots');//через 1мске приду в начало run()
                return;
            }
            var slot = this.aSlots[index];//получить текущий слот
            if (slot.settings.Change) {//слот вернулся из onDataRead с приказом переключится на следующий слот
                slot.settings.Change = false;//готовлю слот к следующему срабатыванию
                //указатель на следующий слот
                index++;//увеличил указатель
                if (index >= this.aSlots.length) index = 0;//если указатель превысил размер массива то принимает 0 значение
                this.timerOutID = setTimeout (this.run.bind(self), slot.settings.BeforeWriteTime, 'ChangeSlot');//через }{мск приду в начало run()
            }
            else {
                slot.settings.Change = false; //признак того что текущий слот не не надо сменить на следующий
                //запуск таймера на отсчёт тайм аута
                this.timerOutID = setTimeout (this.onDataRead.bind(self), slot.settings.TimeOut, 'ErrTimeOut');
                //запись в порт и ожидание входа в onDataRead
                this.port.sp.write(Buffer.from(slot.out));
                this.port.sp.drain();//жду чтобы все данные были переданы
            }
        }
    }
}

var crc = require('./crc16');
var SerialPort = require('serialport');
//Класс сериал порт
//отдаю его в класс TLnkSlot в объект comm
function TSerialPort (settings) {
    //console.log(SerialPort);
    console.log('available COM-ports');
    SerialPort.list(function (err, ports) {
        ports.forEach(function(port) {
            console.log(port.comName);
            console.log(port.pnpId);
            console.log(port.manufacturer);
        });
    });

    this.isOpen = false;
    this.sp = new SerialPort(settings.port, settings.settings // portName is instatiated to be COM3, replace as necessary
      //settings.baudRate: 115200,
      //dataBits: 8,
      //parity: 'none',
      //stopBits: 1,
      //flowControl: false
    );
    console.log(this.sp);
    
    //var baMsg = [0x01,0x11];//запрос строки идентификатора
    //var baMsg = crc.addCRC16toFrame([0x01,0x11]);//добавляю CRC в конец пакета
    //    console.log (baMsg);//смотрю что получилось   
    //обработчики событий
    this.onError = function (err){
        console.log('TSerialPort.Error: ', err.message);
    }

    this.onClose = function (){
        console.log('TSerialPort.Serial port is closed');
        this.isOpen = false;//порт закрыт, низя песать внего и четать из нево
    }

    this.onOpen = function (){
        console.log('TSerialPort.Serial port is opened');
        //this.sp.write(Buffer.from(baMsg));
        this.isOpen = true;//порт открыт можно работать
    }

    var self = this;
    //установить обработчики событий
    this.sp.on('open',  this.onOpen.bind(self));
    this.sp.on('close', this.onClose.bind(self));
    this.sp.on('error', this.onError.bind(self));

}