module.exports.THTTPServer = THTTPServer;
module.exports.getExample = getExample;
var express = require('express');
var bodyParser = require('body-parser');

function THTTPServer(p) {
    this.app = express();
    var port = process.env.PORT || p;
    this.app.set('port', port);
    this.app.listen(this.app.get('port'), () => {
        console.log('TServer.listen port:' + port);
      });
    
      //чтобы с LocalHost можно было обращаться к серверу
      //из Java Script на стороне front-end
      //в заголовок пакета надо добавить "Access-Control-Allow-Origin", "*"
      //это можно сделать как тут через app.use
      //app.use(function(req, res, next) {
      //  res.header("Access-Control-Allow-Origin", "*");
      //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      //  next();
      //});
      //или непосредственно перед ответом
      //response.setHeader('Access-Control-Allow-Origin','*');
      //response.send("ответ клиенту");
      /*
      this.app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });
      */
      //Для выдачи статических файлов из директории public
      this.app.use(express.static('public'));
      //
      this.app.use(bodyParser.json());//использую БодиПарсер для разбора структур GET и POST запросов

    var self = this;
    //добавляет маршрут для прослушивания
    this.addGet = function (route, callback) {
        this.app.get(route, callback);
    }
}

var count = 0;
function getExample (req, response) {
    var s = JSON.stringify(req.query);
    console.log(s);
    count++;
    //и так можно дабавить 'Access-Control-Allow-Origin','*'
    //в заголовок
    //response.setHeader('Access-Control-Allow-Origin','*');

    response.send("<h1>Главная страница:"+count+"</h1>");//('>'+s);//"<h1>Главная страница</h1>");
}

/*
app.get('/', function(request, response){
     
            response.send("<h1>Главная страница</h1>");
        });
app.get("/about", function(request, response){
     
    response.send("<h1>О сайте</h1>");
});
app.get("/contact", function(request, response){
     
    response.send("<h1>Контакты</h1>");
});
*/